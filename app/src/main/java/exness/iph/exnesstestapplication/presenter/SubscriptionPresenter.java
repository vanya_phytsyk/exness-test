package exness.iph.exnesstestapplication.presenter;

import java.util.Set;

import exness.iph.exnesstestapplication.repository.LocalTicksRepository;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.view.SubscriptionView;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class SubscriptionPresenter {

    private LocalTicksRepository localTicksRepository;
    private SubscriptionView subscriptionView;

    public SubscriptionPresenter(SubscriptionView subscriptionView, LocalTicksRepository localTicksRepository) {
        this.localTicksRepository = localTicksRepository;
        this.subscriptionView = subscriptionView;
    }

    public void start() {
        Set<CurrencyPair> savedState = localTicksRepository.getLastSavedSubscriptions();
        subscriptionView.showSubscriptions(savedState);
    }

    public void saveClicked(Set<CurrencyPair> subscriptionSet) {
        localTicksRepository.saveState(subscriptionSet);
        subscriptionView.moveBack();
    }
}
