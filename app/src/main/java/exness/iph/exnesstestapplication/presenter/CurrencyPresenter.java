package exness.iph.exnesstestapplication.presenter;

import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.LocalTicksRepository;
import exness.iph.exnesstestapplication.repository.RemoteTicksRepository;
import exness.iph.exnesstestapplication.repository.TickCallback;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;
import exness.iph.exnesstestapplication.view.CurrencyView;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public class CurrencyPresenter {
    private final CurrencyView currencyView;
    private LocalTicksRepository localTicksRepository;
    private RemoteTicksRepository remoteTicksRepository;

    public CurrencyPresenter(CurrencyView currencyView, LocalTicksRepository localTicksRepository, RemoteTicksRepository remoteTicksRepository) {
        this.currencyView = currencyView;
        this.localTicksRepository = localTicksRepository;
        this.remoteTicksRepository = remoteTicksRepository;
    }

    public void start() {
        List<Tick> savedTicks = localTicksRepository.getTicks();
        currencyView.showTicks(savedTicks);
        Set<CurrencyPair> subscriptionSet = localTicksRepository.getLastSavedSubscriptions();
        remoteTicksRepository.subscribe(subscriptionSet, tickCallback);
    }

    private TickCallback tickCallback = new TickCallback() {
        @Override
        public void onTickReceived(List<Tick> ticks) {
            processNewTicks(ticks);
        }
    };

    private void processNewTicks(List<Tick> ticks) {
        localTicksRepository.saveTicks(ticks);
        currencyView.showTicks(ticks);
    }

    public void stop() {
        localTicksRepository.saveTicks(currencyView.getOrderedTicks());
        remoteTicksRepository.unsubscribe();
    }

    public void subscriptionsButtonClicked() {
        currencyView.showSubscriptions();
    }

    public void itemClicked(CurrencyPair currencyPair) {
        currencyView.showChart(currencyPair);
    }

    public void itemDeleted(CurrencyPair currencyPair) {
        Set<CurrencyPair> currencyPairs = localTicksRepository.getLastSavedSubscriptions();
        currencyPairs.remove(currencyPair);
        localTicksRepository.saveState(currencyPairs);
        remoteTicksRepository.subscribe(currencyPairs, tickCallback);
    }
}
