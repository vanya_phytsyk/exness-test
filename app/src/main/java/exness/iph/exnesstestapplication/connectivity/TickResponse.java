package exness.iph.exnesstestapplication.connectivity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class TickResponse {
    @SerializedName("subscribed_count")
    private int subscribedCount;

    @SerializedName("subscribed_list")
    private SubscribedList subscribedList;

    public int getSubscribedCount() {
        return subscribedCount;
    }

    public void setSubscribedCount(int subscribedCount) {
        this.subscribedCount = subscribedCount;
    }

    public SubscribedList getSubscribedList() {
        return subscribedList;
    }

    public void setSubscribedList(SubscribedList subscribedList) {
        this.subscribedList = subscribedList;
    }
}
