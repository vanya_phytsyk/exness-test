package exness.iph.exnesstestapplication.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import exness.iph.exnesstestapplication.R;
import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public class CurrencyRow extends LinearLayout {

    private TextView currencySymbol;
    private TextView bidAsk;
    private TextView spread;

    private Tick tick;

    public CurrencyRow(Context context) {
        super(context);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        View root = inflate(getContext(), R.layout.widget_table_row, this);
        currencySymbol = (TextView) root.findViewById(R.id.tv_symbol);
        bidAsk = (TextView) root.findViewById(R.id.tv_bid_ask);
        spread = (TextView) root.findViewById(R.id.tv_spread);
    }


    public void bindTick(Tick tick, boolean isEvenPosition) {
        if (!tick.equals(this.tick)) { //we do not need to make formatting if currency pair is the same
            currencySymbol.setText(getSymbolString(tick.getCurrencyPair().name()));
        }
        bidAsk.setText(String.format(Locale.getDefault(), "%s / %s", tick.getAsk(), tick.getBid()));
        spread.setText(String.valueOf(tick.getSpread()));
        int color = getResources().getColor(isEvenPosition ? R.color.colorEvenItem : R.color.colorOddItem);
        setBackgroundColor(color);
        this.tick = tick;
    }

    private String getSymbolString(String symbol) {
        return symbol.substring(0, 3).concat("/").concat(symbol.substring(3)); //could be better
    }
}
