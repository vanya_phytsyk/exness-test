package exness.iph.exnesstestapplication.ui;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public interface ItemTouchHelperAdapter {
    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}