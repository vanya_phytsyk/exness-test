package exness.iph.exnesstestapplication.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import exness.iph.exnesstestapplication.repository.LocalTicksRepository;
import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public class PreferenceManager implements LocalTicksRepository {

    private static final String KEY_LAST_SAVED_STATE = "last_saved_state";
    private static final String KEY_TICKS = "ticks";
    private static final Set<CurrencyPair> DEFAULT_SUBSCRIPTION_SET = new HashSet<>(Arrays.asList(CurrencyPair.EURUSD, CurrencyPair.GBPUSD, CurrencyPair.EURJPY)); //for example

    private Gson gson = new GsonBuilder().create();
    private Type subscriptionSetType = new TypeToken<Set<CurrencyPair>>() {
    }.getType();
    private Type ticksListType = new TypeToken<List<Tick>>() {
    }.getType();
    private SharedPreferences sharedPreferences;

    public PreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences("ExnessPref", Context.MODE_PRIVATE);
    }

    @Override
    public Set<CurrencyPair> getLastSavedSubscriptions() {
        String savedJson = sharedPreferences.getString(KEY_LAST_SAVED_STATE, "");
        if (TextUtils.isEmpty(savedJson)) {
            return DEFAULT_SUBSCRIPTION_SET;
        } else {
            return gson.fromJson(savedJson, subscriptionSetType);
        }
    }

    @Override
    public void saveState(Set<CurrencyPair> currencyPairs) {
        String json = gson.toJson(currencyPairs);
        sharedPreferences.edit().putString(KEY_LAST_SAVED_STATE, json).commit();
    }

    @Override
    public double getLastSavedCurrency(CurrencyPair currencyPair) {
        return 0;
    }

    @Override
    public void saveCurrency(CurrencyPair currencyPair, double value) {

    }

    @Override
    public void saveTicks(List<Tick> ticks) {
        String json = gson.toJson(ticks);
        sharedPreferences.edit().putString(KEY_TICKS, json).apply();
    }

    @Override
    public List<Tick> getTicks() {
        String json = sharedPreferences.getString(KEY_TICKS, "");
        if (TextUtils.isEmpty(json)) {
            return Collections.emptyList();
        } else {
            return gson.fromJson(json, ticksListType);
        }
    }
}
