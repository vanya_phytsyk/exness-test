package exness.iph.exnesstestapplication.view;

import java.util.List;

import exness.iph.exnesstestapplication.repository.model.CurrencyPair;
import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public interface CurrencyView {
    void showTicks(List<Tick> ticks);

    void showSubscriptions();

    List<Tick> getOrderedTicks();

    void showChart(CurrencyPair currencyPair);
}
