package exness.iph.exnesstestapplication.view;

import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/30/17.
 */

public interface ChartView {
    void addPointToChart(Tick newBidAsk);
}
