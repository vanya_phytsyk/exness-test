package exness.iph.exnesstestapplication.repository.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public enum CurrencyPair {
    @SerializedName("EURUSD")
    EURUSD,
    @SerializedName("EURGBP")
    EURGBP,
    @SerializedName("USDJPY")
    USDJPY,
    @SerializedName("GBPUSD")
    GBPUSD,
    @SerializedName("USDCHF")
    USDCHF,
    @SerializedName("USDCAD")
    USDCAD,
    @SerializedName("AUDUSD")
    AUDUSD,
    @SerializedName("EURJPY")
    EURJPY,
    @SerializedName("EURCHF")
    EURCHF
}
