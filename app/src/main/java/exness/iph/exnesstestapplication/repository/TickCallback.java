package exness.iph.exnesstestapplication.repository;

import java.util.List;

import exness.iph.exnesstestapplication.repository.model.Tick;

/**
 * Created by ivanphytsyk on 5/29/17.
 */

public interface TickCallback {
    void onTickReceived(List<Tick> ticks);
}
